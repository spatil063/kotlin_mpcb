package com.example.mpcb.task_management


import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mpcb.R
import com.example.mpcb.addTask.AddTaskFragment
import com.example.mpcb.base.BaseFragment
import com.example.mpcb.databinding.FragmentTaskMngtBinding
import com.example.mpcb.utils.addFragment
import com.example.mpcb.utils.showMessage


class TaskManagementFragment : BaseFragment<FragmentTaskMngtBinding, TaskManagementViewModel>()
    , TaskManagementNavigator {

    override fun getLayoutId() = R.layout.fragment_task_mngt
    override fun getViewModel() = TaskManagementViewModel::class.java
    override fun getNavigator() = this@TaskManagementFragment
    override fun onError(message: String) = showMessage(message)
    override fun onInternetError() {}

    override fun onBinding() {
        setToolbar(
            mBinding.toolbarLayout,
            getString(R.string.task_mngt_title),
            showSearchBar = true
        )
        mBinding.toolbarLayout.imgCalendar.visibility = View.GONE
        setUpRecyclerView()
        setFloatingActionButton()
    }

    private fun setFloatingActionButton() {
        val bundle = Bundle()
        mBinding.fabTaskManagement.setOnClickListener {
            addFragment(AddTaskFragment(), false, bundle)
        }
    }

    private fun setUpRecyclerView() {
        mBinding.rvTasks.layoutManager = LinearLayoutManager(getBaseActivity())
        val adapter = TaskMngtAdapter(getBaseActivity(), mViewModel)
        mBinding.rvTasks.adapter = adapter
    }
}
