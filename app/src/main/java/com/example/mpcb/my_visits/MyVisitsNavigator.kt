package com.example.mpcb.my_visits

import com.example.mpcb.base.BaseNavigator
import com.example.mpcb.network.response.CheckInfoModel
import com.example.mpcb.network.response.MyVisitModel
import com.example.mpcb.network.response.Users

interface MyVisitsNavigator : BaseNavigator {

    fun onVisitItemClicked(viewModel: MyVisitModel)

    fun onCheckInClicked(model: MyVisitModel)

    fun onCheckInSuccess(msg: String)

    fun dismissCheckinDialog()

    fun showAlert(message: String)

    fun onAlreadyCheckedIn(model: CheckInfoModel)

    //Sets the data in Spinner
    fun setSpinnerData(users: List<Users>)
}